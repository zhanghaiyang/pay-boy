package com.ndood.admin.service.system;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.LogPo;
import com.ndood.admin.pojo.system.query.LogQuery;

/**
 * 日志管理业接口
 */
public interface SystemLogService {

	/**
	 * 日志列表
	 */
	DataTableDto pageLogList(LogQuery query) throws Exception;

	void makeLog(LogPo log);
	
}
