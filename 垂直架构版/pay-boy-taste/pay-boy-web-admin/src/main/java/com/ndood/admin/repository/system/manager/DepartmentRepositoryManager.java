package com.ndood.admin.repository.system.manager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ndood.admin.pojo.system.dto.DepartmentDto;

@SuppressWarnings("unchecked")
@Repository
public class DepartmentRepositoryManager {
	
	@Autowired
	@PersistenceContext
	private EntityManager em;
	
	public List<DepartmentDto> getDepartmentList() {
		Query query = em.createQuery("SELECT new com.ndood.admin.pojo.system.dto.DepartmentDto(d.id, d.name, d.sort, d.status, d.createTime, d.updateTime, p.id as parentId) "
				+ "FROM DepartmentPo d LEFT JOIN d.parent p ON 1 = 1 "
				+ "WHERE 1 = 1 "
				+ "ORDER BY d.sort ASC ");
		
		query.setHint("org.hibernate.cacheable", true);
		List<DepartmentDto> resultList = query.getResultList();
		return resultList;
	}
	
}
