package com.ndood.admin.pojo.system.dto;

import java.io.Serializable;
import java.util.Date;

import com.ndood.admin.pojo.system.JobPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class JobDto extends JobPo implements Serializable {
	private static final long serialVersionUID = 4602771890574682756L;
	
	public JobDto(Integer id, String cronExpression, String methodName, String isConcurrent, String description,
			String updateBy, String beanClass, String jobStatus, String jobGroup, String createBy, String springBean,
			String jobName, Date createTime, Date updateTime) {
		super();
		this.setId(id);
		this.setCronExpression(cronExpression);
		this.setMethodName(methodName);
		this.setIsConcurrent(isConcurrent);
		this.setDescription(description);
		this.setUpdateBy(updateBy);
		this.setBeanClass(beanClass);
		this.setJobStatus(jobStatus);
		this.setJobGroup(jobGroup);
		this.setCreateBy(createBy);
		this.setSpringBean(springBean);
		this.setJobName(jobName);
		this.setCreateTime(createTime);
		this.setUpdateTime(updateTime);
	}
	
	public JobDto() {
		super();
	}
	
}