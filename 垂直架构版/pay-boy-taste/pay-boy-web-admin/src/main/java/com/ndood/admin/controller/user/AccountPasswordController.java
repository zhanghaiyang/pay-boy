package com.ndood.admin.controller.user;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.core.constaints.AdminErrCode;
import com.ndood.admin.core.exception.AdminException;
import com.ndood.admin.core.web.tools.SessionUtils;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.service.user.AccountPasswordService;

/**
 * 账户密码管理
 */
@Controller
public class AccountPasswordController {
	
	@Autowired
	private SessionUtils sessionUtils;
	
	@Autowired
	private AccountPasswordService accountPasswordService;
	
	@GetMapping("/user/account/password")
	public String toPassword(){
		return "user/account/password/account_password";
	}
	
	@PostMapping("/user/account/password/update")
	@ResponseBody
	public AdminResultVo updatePassword(String old_password, String new_password, String reply_password) throws AdminException {
		
		// Step1: 参数校验
		if(StringUtils.isBlank(old_password)||StringUtils.isBlank(new_password)||StringUtils.isBlank(reply_password)){
			throw new AdminException(AdminErrCode.ERR_PARAM, "请求参数不能为空");
		}
		if(!new_password.equals(reply_password)) {
			throw new AdminException(AdminErrCode.ERR_PARAM, "两次输入的密码不一致");
		}
		
		// Step2: 修改密码
		UserDetails userInfo = sessionUtils.getLoginUserInfo();
		if(userInfo==null) {
			throw new AdminException(AdminErrCode.ERR_OTHER, "用户未登录");
		}
		
		String userId = userInfo.getUsername();
		accountPasswordService.changePassword(userId, old_password, new_password);
		return AdminResultVo.ok().setMsg("修改密码成功，建议退出后重新登录试试");
		
	}
}
