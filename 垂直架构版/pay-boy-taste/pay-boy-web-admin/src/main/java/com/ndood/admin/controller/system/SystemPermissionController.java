package com.ndood.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.PermissionPo;
import com.ndood.admin.pojo.system.dto.PermissionDto;
import com.ndood.admin.service.system.SystemPermissionService;

/**
 * 资源控制器类
 * @author ndood
 */
@Controller
public class SystemPermissionController {
	
	@Autowired
	private SystemPermissionService systemPermissionService;

	/**
	 * 显示资源页
	 */
	/*@PreAuthorize("hasAuthority('aaa')")*/
	@GetMapping("/system/permission")
	public String toPermissionPage(){
		return "system/permission/permission_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/permission/add")
	public String toAddPermission(Integer parentId,String parentName, Model model){
		if(!StringUtils.isEmpty(parentId)&&!StringUtils.isEmpty(parentName)){
			model.addAttribute("parentId", parentId);
			model.addAttribute("parentName", parentName);
		}
		return "system/permission/permission_add";
	}
	
	/**
	 * 添加资源
	 */
	@PostMapping("/system/permission/add")
	@ResponseBody
	public AdminResultVo addPermission(@RequestBody PermissionDto permission) throws Exception{
		PermissionDto obj = systemPermissionService.addPermission(permission);
		return AdminResultVo.ok().setData(obj).setMsg("添加资源成功！");
	}
	
	/**
	 * 展示所有资源
	 */
	@GetMapping("/system/permission/treeview")
	@ResponseBody
	public List<PermissionDto> getPermissionTreeview() throws Exception{
		List<PermissionDto> permissionList = systemPermissionService.getPermissionList();
		return permissionList;
	}
	
	/**
	 * 删除资源
	 */
	@PostMapping("/system/permission/delete")
	@ResponseBody
	public AdminResultVo deletePermission(Integer id){
		Integer[] ids = new Integer[]{id};
		systemPermissionService.batchDeletePermission(ids);
		return AdminResultVo.ok().setMsg("删除资源成功！");
	}
	
	/**
	 * 批量删除资源
	 */
	@PostMapping("/system/permission/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeletePermission(@RequestParam("ids[]") Integer[] ids){
		systemPermissionService.batchDeletePermission(ids);
		return AdminResultVo.ok().setMsg("批量删除资源成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/permission/update")
	public String toUpdatePermission(Integer id, Model model) throws Exception{
		PermissionPo permission = systemPermissionService.getPermission(id);
		model.addAttribute("permission", permission);
		return "system/permission/permission_update";
	}
	
	/**
	 * 修改资源
	 */
	@PostMapping("/system/permission/update")
	@ResponseBody
	public AdminResultVo updatePermission(@RequestBody PermissionDto permission) throws Exception{
		PermissionDto obj = systemPermissionService.updatePermission(permission);
		return AdminResultVo.ok().setData(obj).setMsg("修改资源成功！");
	}
}
