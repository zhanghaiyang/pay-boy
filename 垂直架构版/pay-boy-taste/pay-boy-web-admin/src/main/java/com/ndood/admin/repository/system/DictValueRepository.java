package com.ndood.admin.repository.system;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.DictValuePo;

/**
 * 数据字典值dao
 * @author ndood
 */
public interface DictValueRepository extends JpaRepository<DictValuePo, Integer>{

}
