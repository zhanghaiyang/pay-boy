package com.ndood.admin.pojo.system.dto;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class ConfigDto implements Serializable{
	
	private static final long serialVersionUID = 3279676515586974879L;

}
