package com.ndood.core.social.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

/**
 * 绑定结果视图
 */
// 为了能够通用，不弄成注解形式
public class ConnectView extends AbstractView {
	
	/**
	 * 渲染视图
	 */
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setContentType("text/html;charset=UTF-8");
		
		// 判断该账号是否重复绑定
		Object social_addConnection_duplicate = model.get("social_addConnection_duplicate");
		// Object social_authorization_error = model.get("social_authorization_error");
		
		if(social_addConnection_duplicate!=null) {
			// response.sendRedirect("/message/" + msg);
			response.getWriter().write("<h3>绑定失败，该社交账号已经绑定其它账号</h3><script>window.opener.refresh_binding_status();window.opener.layer.alert('绑定失败，该社交账号已经绑定其它账号',{icon:2});var index = window.opener.layer.getFrameIndex(window.name);window.close();</script>");
		} else if (model.get("connections") == null) {
			response.getWriter().write("<h3>解绑成功</h3><script>window.opener.refresh_binding_status();window.opener.layer.alert('解绑成功',{icon:1});var index = window.opener.layer.getFrameIndex(window.name);window.close();</script>");
		} else {
			response.getWriter().write("<h3>绑定成功</h3><script>window.opener.refresh_binding_status();window.opener.layer.alert('绑定成功',{icon:1});var index = window.opener.layer.getFrameIndex(window.name);window.close();</script>");
		}
		response.getWriter().flush();
	}
	
}
